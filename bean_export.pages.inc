<?php

function bean_export_bean_export_page() {
  $beanTypes = bean_get_types();
  $items = array();
  foreach ($beanTypes as $beanType) {
    $items[] = l($beanType->type, 'admin/content/blocks/bean-export/' . $beanType->type);
  }
  return array(
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#title' => '',
    '#attributes' => array('class' => array('admin-list')),
    '#items' => $items
  );
}

function bean_export_execute_form($form, &$form_state) {
  $form_state['bean_type'] = $form_state['build_info']['args'][1];
  // Fildset actions
  $form['actions'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#title' => t('Actions')
  );
  // Submit button
  $form['actions']['export'] = array(
    '#type' => 'submit',
    '#value' => t('Export'),
    '#submit' => array('bean_export_execute_form_submit_export')
  );
  // Table bean to user select
  if ($form_state['build_info']['args'][0]) {
    $rows = array();
    foreach ($form_state['build_info']['args'][0] as $bean) {
      $rows[$bean->delta] = array(l($bean->label, 'block/' . $bean->delta), $bean->type, $bean->delta);
    }
    $form['bean_export'] = array(
      '#type' => 'tableselect',
      '#header' => array('Title', 'Type', 'Delta'),
      '#options' => $rows
    );
  }
  // Validate
  $form['#validate'][] = 'bean_export_execute_form_validate';
  return $form;
}

function bean_export_execute_form_validate($form, &$form_state) {
  $form_state['values']['bean_export'] = array_filter($form_state['values']['bean_export'], function($e) {
    return is_string($e);
  });
  if (empty($form_state['values']['bean_export'])) {
    form_set_error('bean_export', t('Please select bean you want to export before.'));
  }
}

function bean_export_execute_form_submit_export($form, &$form_state) {
  at_id(new Drupal\bean_export\Controllers\ExportOutput($form_state))->execute();
}

function bean_export_bean_export_type_page($bean_type) {
  drupal_set_title(t('Export block type @type', array('@type' => $bean_type->type)));
  $query = new EntityFieldQuery();
  $result = $query
    ->entityCondition('entity_type', 'bean')
    ->propertyCondition('type', $bean_type->type)
    ->execute();
  if (isset($result['bean'])) {
    return drupal_get_form('bean_export_execute_form', bean_load_multiple(array_keys($result['bean'])), $bean_type->type);
  }
  return array('#markup' => t('There are no beans for this block type.'));
}

function bean_export_download_latest_export() {
  if (isset($_SESSION['bean_latest_export']) && is_file($_SESSION['bean_latest_export'])) {
    ob_get_clean();
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private", false);
    header("Content-Type: application/zip");
    header("Content-Disposition: attachment; filename=" . basename($_SESSION['bean_latest_export']) . ";");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($_SESSION['bean_latest_export']));
    readfile($_SESSION['bean_latest_export']);
    exit;
  }
  return 'There are no any export.';
}
