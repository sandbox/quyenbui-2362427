<?php

namespace Drupal\bean_export\Controllers;

class BuildFormatExport
{

  private $form_state, $beanSelected;

  public function __construct($form_state)
  {
    $this->form_state = $form_state;
    $this->beanSelected = $this->form_state['values']['bean_export'];
  }

  public function build()
  {
    $this->getBeanObject();
    $fieldTypes = $this->getFieldInfo();
    $fieldNames = array_keys($fieldTypes);
    $objectImport = array();
    foreach ($this->getBeanObject() as $bean) {
      // Get fields to process
      foreach ($fieldNames as $fieldName) {
        if (!empty($bean->{$fieldName})) {
          $objectImport[$bean->delta]['fields'][$fieldName] = at_id(new \Drupal\bean_export\Controllers\FieldProcess($fieldTypes[$fieldName], $bean->{$fieldName}))->getValue();
        }
        unset($bean->{$fieldName});
      }
      // Bean properties
      foreach ($bean as $key => $value) {
        $objectImport[$bean->delta]['properties'][$key] = $value;
      }
    }

    return $objectImport;
  }

  private function getBeanObject()
  {
    $beans = array();
    foreach ($this->beanSelected as $delta) {
      if (($bean = bean_load($delta))) {
        $beans[] = $bean;
      }
    }

    return $beans;
  }

  private function getFieldInfo()
  {
    $bean_fields = field_info_instances('bean');
    $fieldType = array();
    foreach (array_shift($bean_fields) as $field) {
      $fieldObject = field_info_field($field['field_name']);
      $fieldType[$field['field_name']] = $fieldObject['type'];
    }

    return $fieldType;
  }

}
