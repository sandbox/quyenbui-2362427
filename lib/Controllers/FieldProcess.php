<?php

namespace Drupal\bean_export\Controllers;

class FieldProcess
{

  private $fieldType = null;
  private $fieldData = null;

  public function __construct($fieldType, $fieldData)
  {
    $this->fieldType = $fieldType;
    $this->fieldData = $fieldData;
  }

  public function getValue()
  {
    $handlers = $this->handlerDefine();
    if (isset($handlers[$this->fieldType]) && is_callable($handlers[$this->fieldType])) {
      return call_user_func_array($handlers[$this->fieldType], array(array(
          'type' => $this->fieldType,
          'data' => $this->fieldData
        ))
      );
    }

    return array();
  }

  private function handlerDefine()
  {
    $handler = array();
    // Text field
    $textFields = array(
      'text', 'text_with_summary', 'number_float', 'number_integer',
      'text_long', 'number_decimal', 'list_boolean', 'list_float',
      'list_integer', 'list_text', 'tablefield', 'link_field'
    );
    foreach ($textFields as $fieldType) {
      $handler[$fieldType] = array($this, 'textField');
    }
    // File field
    $fileFields = array('file', 'image');
    foreach ($fileFields as $fieldType) {
      $handler[$fieldType] = array($this, 'fileField');
    }

    return $handler;
  }

  public function textField($fieldInfo)
  {
    return $fieldInfo;
  }

  public function fileField($fieldInfo)
  {
    $fieldInfo['is_file'] = true;
    foreach ($fieldInfo['data'] as $language => $values) {
      foreach ($values as $delta => $value) {
        $fieldInfo['data'][$language][$delta] = array();
        $fieldInfo['data'][$language][$delta]['file_info'] = $value;
      }
    }

    return $fieldInfo;
  }

}
