<?php

namespace Drupal\bean_export\Controllers;

class ExportOutput
{

  /**
   * Directory storage file after exported
   */
  private $directory = 'temporary://bean_export';
  private $beanData;
  private $seesionDirectory;

  public function __construct($form_state)
  {
    $this->beanData = at_id(new \Drupal\bean_export\Controllers\BuildFormatExport($form_state))->build();
    $this->seesionDirectory = time();
  }

  public function execute()
  {
    $this->folderInit();
    $this->entityObjectOutput();
  }

  private function entityObjectOutput()
  {
    $this->removePropertyNoNeed();
    $this->fileFieldProccess();
    $this->entityDataPut();
    // Output as zip compress
    $zipName = tempnam(drupal_realpath('temporary://'), 'bean_export-' . date('d-m-Y', time()) . '-') . '.zip';
    $sourcePath = drupal_realpath($this->getSessionDirectoryUri());
    $zip = at_container('bean_export.zip_compress');
    if ($zip->open($zipName, \ZipArchive::CREATE) === TRUE) {
      $zip->addDir($sourcePath);
      $zip->close();
      $_SESSION['bean_latest_export'] = $zipName;
      drupal_set_message('Your selected has been exported.');
    } else {
      drupal_set_message('Could not create a zip archive.', 'error');
    }
  }

  private function fileFieldProccess()
  {
    foreach ($this->beanData as &$value) {
      foreach ($value['fields'] as &$field) {
        if (isset($field['is_file']) && $field['is_file']) {
          $field['data'] = $this->_fileFieldDataProccess($field['data']);
        }
      }
    }
  }

  private function _fileFieldDataProccess($data)
  {
    foreach ($data as &$value) {
      foreach ($value as &$finfo) {
        // Copy file
        $fileDest = 'files/' . substr(md5($finfo['file_info']['uri']), 0, 10) . ' ' . $finfo['file_info']['filename'];
        copy(drupal_realpath($finfo['file_info']['uri']), $this->getSessionDirectoryUri() . '/' . $fileDest);
        $finfo['file'] = $fileDest;
        unset($finfo['file_info']['uid'], $finfo['file_info']['uri']);
      }
    }

    return $data;
  }

  private function entityDataPut()
  {
    file_put_contents(drupal_realpath($this->getSessionDirectoryUri() . '/entity.serialize.bin'), serialize($this->beanData));
  }

  private function removePropertyNoNeed()
  {
    foreach ($this->beanData as $delta => $value) {
      foreach (array('bid', 'vid', 'created', 'changed', 'default_revision', 'uid', 'revisions', 'log') as $property) {
        unset($this->beanData[$delta]['properties'][$property]);
      }
    }
  }

  private function folderInit()
  {
    if (!is_dir(drupal_realpath($this->directory)) && !drupal_mkdir($this->directory, 0777)) {
      throw new \Exception(t('Can not create new required directory @directory', array('@directory' => drupal_realpath($this->directory))));
    }
    if (!drupal_mkdir($this->getSessionDirectoryUri(), 0777) || !drupal_mkdir($this->getSessionDirectoryUri() . '/files', 0777)) {
      throw new \Exception(t('Can not create new required directory'));
    }
  }

  private function getSessionDirectoryUri()
  {
    return $this->directory . '/' . $this->seesionDirectory;
  }

}
